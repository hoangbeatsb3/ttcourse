package repository

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserRepo struct {
	db         *mgo.Database
	collection string
}

func NewUserRepo(cfg *config.Config) (*UserRepo, error) {
	logrus.Infof("main: configuring NoSQL")
	sess, err := mgo.Dial(cfg.Database.URL)
	sess.SetMode(mgo.Monotonic, true)
	if err != nil {
		return nil, err
	}
	return &UserRepo{
		db:         sess.DB(cfg.Database.Database),
		collection: cfg.Database.UserCollection,
	}, nil
}

func (rr *UserRepo) Create(r *model.UserCollection) (*model.UserCollection, error) {
	c := rr.db.C(rr.collection)
	err := c.Insert(r)
	return r, err
}

func (rr *UserRepo) GetUserByEmail(r string) (model.UserCollection, error) {
	ret := model.UserCollection{}
	err := rr.db.C(rr.collection).Find(bson.M{"email": r}).One(&ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func (rr *UserRepo) FindUserByCourse(email []string) ([]model.UserCollection, error) {
	c := rr.db.C(rr.collection)
	ret := make([]model.UserCollection, 0)
	err := c.Find(bson.M{"email": bson.M{"$in": email}}).All(&ret)
	return ret, err
}
