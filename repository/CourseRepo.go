package repository

import (
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type CourseRepo struct {
	db         *mgo.Database
	collection string
}

func NewCourseRepo(cfg *config.Config) (*CourseRepo, error) {
	logrus.Infof("main: configuring NoSQL")
	sess, err := mgo.Dial(cfg.Database.URL)
	sess.SetMode(mgo.Monotonic, true)
	if err != nil {
		return nil, err
	}
	return &CourseRepo{
		db:         sess.DB(cfg.Database.Database),
		collection: cfg.Database.CourseCollection,
	}, nil
}

func (rr *CourseRepo) GetCourseByName(r string) (model.CourseCollection, error) {
	ret := model.CourseCollection{}
	err := rr.db.C(rr.collection).Find(bson.M{"coursename": r}).One(&ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func (rr *CourseRepo) GetCourseByNameIgnoreCase(r string) ([]model.CourseCollection, error) {
	ret := make([]model.CourseCollection, 0)
	regex := bson.M{"$regex": bson.RegEx{Pattern: `^` + r + `.*`, Options: "i"}}
	err := rr.db.C(rr.collection).Find(bson.M{"coursename": regex}).All(&ret)

	return ret, err
}

func (rr *CourseRepo) Create(r *model.CourseCollection) (*model.CourseCollection, error) {
	c := rr.db.C(rr.collection)
	err := c.Insert(r)
	return r, err
}

func (rr *CourseRepo) FindCoursesNonRegistered(coursename []string) ([]model.CourseCollection, error) {
	c := rr.db.C(rr.collection)
	ret := make([]model.CourseCollection, 0)
	err := c.Find(bson.M{"coursename": bson.M{"$nin": coursename}}).All(&ret)
	return ret, err
}

func (rr *CourseRepo) FindCoursesRegistered(coursename []string) ([]model.CourseCollection, error) {
	c := rr.db.C(rr.collection)
	ret := make([]model.CourseCollection, 0)
	err := c.Find(bson.M{"coursename": bson.M{"$in": coursename}}).All(&ret)
	return ret, err
}

func (rr *CourseRepo) FindAllCourses() ([]model.CourseCollection, error) {
	ret := make([]model.CourseCollection, 0)
	err := rr.db.C(rr.collection).Find(bson.M{}).All(&ret)
	return ret, err
}

func (rr *CourseRepo) UpdateVote(p model.CourseCollection) (model.CourseCollection, error) {
	err := rr.db.C(rr.collection).Update(
		bson.M{"coursename": p.CourseName},
		bson.M{"$set": bson.M{"vote": p.Vote}},
	)
	logrus.Info("--- Error: ", err)
	if err != nil && strings.Contains(err.Error(), "not found") {
		return p, err
	}
	logrus.Info("--- MongoDB: Update successfully")
	return p, nil
}

func (rr *CourseRepo) FindHighestVote() (model.CourseCollection, error) {
	ret := model.CourseCollection{}
	err := rr.db.C(rr.collection).Find(bson.M{}).Sort("-vote").Limit(1).One(&ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func (rr *CourseRepo) DeleteCourseByName(coursename string) (*model.CourseCollection, error) {
	tmp, err := rr.GetCourseByName(coursename)
	if err != nil {
		return nil, err
	}
	return &tmp, rr.db.C(rr.collection).Remove(bson.M{"coursename": coursename})
}
