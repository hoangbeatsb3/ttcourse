package repository

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type UserVotedCourseRepo struct {
	db         *mgo.Database
	collection string
}

func NewUserVotedCourseRepo(cfg *config.Config) (*UserVotedCourseRepo, error) {
	logrus.Infof("main: configuring NoSQL")
	sess, err := mgo.Dial(cfg.Database.URL)
	sess.SetMode(mgo.Monotonic, true)
	if err != nil {
		return nil, err
	}
	return &UserVotedCourseRepo{
		db:         sess.DB(cfg.Database.Database),
		collection: cfg.Database.UserVotedCourseCollection,
	}, nil
}

func (rr *UserVotedCourseRepo) Create(r *model.UserVotedCourseCollection) (*model.UserVotedCourseCollection, error) {
	c := rr.db.C(rr.collection)
	err := c.Insert(r)
	return r, err
}

func (rr *UserVotedCourseRepo) FindUserVotedCourseByUser(user string) ([]string, error) {
	c := rr.db.C(rr.collection)
	var ret []model.UserVotedCourseCollection
	var result []string
	err := c.Find(bson.M{"email": user}).Select(bson.M{"coursename": 1}).All(&ret)
	if err != nil {
		return nil, err
	}
	for _, v := range ret {
		result = append(result, v.CourseName)
	}
	return result, nil
}

func (rr *UserVotedCourseRepo) FindUserVotedCourseByCourse(coursename string) ([]string, error) {
	c := rr.db.C(rr.collection)
	var ret []model.UserVotedCourseCollection
	err := c.Find(bson.M{"coursename": coursename}).Select(bson.M{"email": 1}).All(&ret)
	if err != nil {
		return nil, err
	}
	var result []string
	for _, v := range ret {
		result = append(result, v.Email)
	}
	return result, nil
}

func (rr *UserVotedCourseRepo) GetUserByEmailCourseName(email string, coursename string) (model.UserVotedCourseCollection, error) {
	ret := model.UserVotedCourseCollection{}
	err := rr.db.C(rr.collection).Find(bson.M{"email": email, "coursename": coursename}).One(&ret)
	if err != nil {
		return ret, err
	}
	return ret, nil
}

func (rr *UserVotedCourseRepo) GetUserVotedCourseByCourseName(coursename string) (*[]model.UserVotedCourseCollection, error) {
	var ret []model.UserVotedCourseCollection
	err := rr.db.C(rr.collection).Find(bson.M{"coursename": coursename}).All(&ret)
	if err != nil {
		return &ret, err
	}
	return &ret, nil
}

func (rr *UserVotedCourseRepo) DeleteUserVotedCourseByCourseName(coursename string) (*[]model.UserVotedCourseCollection, error) {
	tmp, err := rr.GetUserVotedCourseByCourseName(coursename)
	if err != nil {
		return nil, err
	}
	logrus.Info("User voted course list: ", tmp)
	_, errRemoveAll := rr.db.C(rr.collection).RemoveAll(bson.M{"coursename": coursename})
	return tmp, errRemoveAll
}

func (rr *UserVotedCourseRepo) DeleteUserVotedCourseById(coursename string, user string) error {
	logrus.Info("Coursename: ", coursename)
	logrus.Info("Email: ", user)
	return rr.db.C(rr.collection).Remove(bson.M{"coursename": coursename, "email": user})
}
