package service

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/model"
	"gitlab.com/fighters-solution/ttcourses/repository"
)

var courseRepo *repository.CourseRepo
var userVotedCourseRepo *repository.UserVotedCourseRepo
var userRepo *repository.UserRepo

type CourseService struct {
	courseRepo          *repository.CourseRepo
	userRepo            *repository.UserRepo
	userVotedCourseRepo *repository.UserVotedCourseRepo
}

func NewCourseService(cfg *config.Config) *CourseService {
	var err error

	courseRepo, err = repository.NewCourseRepo(cfg)
	if err != nil {
		panic(err)
	}
	userVotedCourseRepo, err = repository.NewUserVotedCourseRepo(cfg)
	if err != nil {
		panic(err)
	}
	userRepo, err = repository.NewUserRepo(cfg)
	if err != nil {
		panic(err)
	}

	return &CourseService{
		userVotedCourseRepo: userVotedCourseRepo,
		userRepo:            userRepo,
		courseRepo:          courseRepo,
	}
}

func (srv *CourseService) FindCourseUserNonRegistered(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "user")
	userVotedCourse, err := userVotedCourseRepo.FindUserVotedCourseByUser(parm)
	HandleError(err)
	var result []model.CourseCollection
	result, err = courseRepo.FindCoursesNonRegistered(userVotedCourse)
	HandleError(err)
	render.JSON(w, r, result)
}

func (srv *CourseService) FindCourseUserRegistered(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "user")
	userVotedCourse, err := userVotedCourseRepo.FindUserVotedCourseByUser(parm)
	HandleError(err)
	var result []model.CourseCollection
	result, err = courseRepo.FindCoursesRegistered(userVotedCourse)
	HandleError(err)
	render.JSON(w, r, result)
}

func (srv *CourseService) FindCourseByNameIgnoreCase(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "alias")
	course, err := courseRepo.GetCourseByNameIgnoreCase(strings.ToLower(parm))
	HandleError(err)
	if err := json.NewEncoder(w).Encode(course); err != nil {
		logrus.Info("Error")
		render.JSON(w, r, err)
	}
}

func (srv *CourseService) FindCoursesByName(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "course_name")
	courses, err := courseRepo.GetCourseByName(parm)
	if err != nil && strings.Contains(err.Error(), "not found") {
		render.JSON(w, r, err)
		panic(err)
		return
	}
	userVotedCourse, err := userVotedCourseRepo.FindUserVotedCourseByCourse(parm)
	if err != nil && strings.Contains(err.Error(), "not found") {
		render.JSON(w, r, err)
		panic(err)
		return
	}
	user, err := userRepo.FindUserByCourse(userVotedCourse)
	if err != nil && strings.Contains(err.Error(), "not found") {
		render.JSON(w, r, err)
		panic(err)
		return
	}
	var courseInfo model.CourseInformation
	courseInfo.Course = courses
	courseInfo.Participants = user
	render.JSON(w, r, courseInfo)
}

func (srv *CourseService) FindAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, err := courseRepo.FindAllCourses()
	HandleError(err)
	if err := json.NewEncoder(w).Encode(courses); err != nil {
		render.JSON(w, r, courses)
	}
}

func (srv *CourseService) FindHighestVote(w http.ResponseWriter, r *http.Request) {
	course, err := courseRepo.FindHighestVote()
	HandleError(err)
	render.JSON(w, r, course)
}

func (srv *CourseService) CreateCourse(w http.ResponseWriter, r *http.Request) {
	var course model.CourseCollection
	body, err := getParams(r)
	HandleError(err)
	if err := r.Body.Close(); err != nil {
		render.JSON(w, r, err)
	}
	if err = json.Unmarshal(body, &course); err != nil {
		if err := json.NewEncoder(w).Encode(err); err != nil {
			render.JSON(w, r, err)
		}
	}
	_, errTmp := courseRepo.GetCourseByName(course.CourseName)
	if errTmp != nil && strings.Contains(errTmp.Error(), "not found") {
		course.Vote = 0
		logrus.Infof("MongoDB: Create new course")
		course.CreatedAt = time.Now()
		courseRepo.Create(&course)
		render.JSON(w, r, course)
	} else {
		logrus.Info("MongoDB: Course already exists")
		render.JSON(w, r, errTmp)
	}
}

func (srv *CourseService) VoteCourse(w http.ResponseWriter, r *http.Request) {
	var course model.CourseCollection
	var userVotedCourse *model.UserVotedCourseCollection
	var user *model.UserCollection
	body, err := ioutil.ReadAll(r.Body)
	HandleError(err)
	// Unmarshal for course model
	if err := json.Unmarshal(body, &course); err != nil {
		render.JSON(w, r, err)
	}
	// Unmarshal for user voted course model
	if err = json.Unmarshal(body, &userVotedCourse); err != nil {
		render.JSON(w, r, err)
	}
	logrus.Info("User voted course: ", userVotedCourse)
	// Unmarshal for user model
	if err = json.Unmarshal(body, &user); err != nil {
		render.JSON(w, r, err)
	}
	logrus.Info("User: ", user)
	var courseTmp model.CourseCollection
	courseTmp, err = courseRepo.GetCourseByName(course.CourseName)
	if err != nil && strings.Contains(err.Error(), "not found") {
		courseRepo.Create(&course)
		return
	}
	_, err = userVotedCourseRepo.GetUserByEmailCourseName(user.Email, course.CourseName)
	if err == nil {
		render.JSON(w, r, err)
		return
	}
	if err != nil && strings.Contains(err.Error(), "not found") {
		courseTmp.Vote += 1
		courseTmp, err = courseRepo.UpdateVote(courseTmp)
		if err != nil {
			render.JSON(w, r, err)
			return
		}
		course.CreatedAt = time.Now()
		userVotedCourseRepo.Create(userVotedCourse)
	}
	_, err = userRepo.GetUserByEmail(user.Email)
	if err != nil && strings.Contains(err.Error(), "not found") {
		user.CreatedAt = time.Now()
		userRepo.Create(user)
	}
	render.JSON(w, r, courseTmp)
}

func (srv *CourseService) UnVoteCourse(w http.ResponseWriter, r *http.Request) {
	var course model.CourseCollection
	var userVotedCourse model.UserVotedCourseCollection
	body, err := ioutil.ReadAll(r.Body)
	HandleError(err)
	// Unmarshal for course model
	if err := json.Unmarshal(body, &course); err != nil {
		render.JSON(w, r, err)
	}
	// Unmarshal for user voted course model
	if err = json.Unmarshal(body, &userVotedCourse); err != nil {
		render.JSON(w, r, err)
	}
	var courseTmp model.CourseCollection
	courseTmp, err = courseRepo.GetCourseByName(course.CourseName)
	if err != nil && strings.Contains(err.Error(), "not found") {
		logrus.Info("Error when course tmp first time")
		render.JSON(w, r, err)
		return
	}
	userVotedCourse, err = userVotedCourseRepo.GetUserByEmailCourseName(userVotedCourse.Email, course.CourseName)
	logrus.Info("User voted course: ", userVotedCourse)
	if err != nil {
		logrus.Info("Error: ", err)
		render.JSON(w, r, nil)
		return
	}
	logrus.Info("User voted course: ", userVotedCourse)
	logrus.Info("Course temp: ", courseTmp)
	courseTmp.Vote -= 1
	courseTmp, err = courseRepo.UpdateVote(courseTmp)
	if err != nil {
		render.JSON(w, r, err)
		return
	}
	logrus.Info("Unvote: ", courseTmp)
	err = userVotedCourseRepo.DeleteUserVotedCourseById(userVotedCourse.CourseName, userVotedCourse.Email)
	if err != nil {
		logrus.Info("Error: ", err)
		render.JSON(w, r, nil)
		return
	}
	render.JSON(w, r, courseTmp)
}

func (srv *CourseService) DeleteCourseByName(w http.ResponseWriter, r *http.Request) {
	parm := chi.URLParam(r, "course_name")
	if err := r.Body.Close(); err != nil {
		render.JSON(w, r, err)
	}
	courseTmp, errTmp := courseRepo.DeleteCourseByName(parm)
	if errTmp != nil {
		logrus.Error("Error: ", errTmp)
		render.JSON(w, r, nil)
		return
	}
	_, errUserVotedCourseTmp := userVotedCourseRepo.DeleteUserVotedCourseByCourseName(parm)
	if errUserVotedCourseTmp != nil {
		logrus.Error("Error: ", errUserVotedCourseTmp)
		render.JSON(w, r, nil)
		return
	}
	render.JSON(w, r, courseTmp)
}

func getParams(r *http.Request) ([]byte, error) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func HandleError(err error) {
	if err != nil {
		panic(err)
	}
}
