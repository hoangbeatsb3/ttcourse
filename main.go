package main

import (
	"gitlab.com/fighters-solution/ttcourses/config"
	"gitlab.com/fighters-solution/ttcourses/controller"
)

func main() {
	cfg := config.LoadEnvConfig()
	controller.Serve(cfg)
}

