package model

import "time"

type (
	UserCollection struct {
		Id        string    `bson:"_id,omitempty" json:"id"`
		UserName  string    `json:"username"`
		Email     string    `json:"email"`
		CreatedAt time.Time `bson:"createdat,omitempty" json:"created_at,omitempty"`
	}

	CourseCollection struct {
		CourseName  string    `bson:"coursename,omitempty" json:"coursename"`
		Description string    `json:"description"`
		Author      string    `json:"author"`
		AuthorEmail string    `json:"authoremail"`
		Image       string    `json:"image"`
		Vote        int       `json:"vote"`
		CreatedAt   time.Time `bson:"createdat,omitempty" json:"created_at,omitempty"`
	}

	UserVotedCourseCollection struct {
		Id         string    `bson:"_id,omitempty" json:"id"`
		CourseName string    `json:"coursename"`
		Email      string    `json:"email"`
		VotedAt    time.Time `bson:"votedat,omitempty" json:"voted_at",omitempty`
	}

	CourseInformation struct {
		Course       CourseCollection `json:"course"`
		Participants []UserCollection `json:"participants"`
	}
)
